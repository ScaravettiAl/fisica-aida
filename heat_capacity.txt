Substance,Phase,Isobaric heat capacity,Isobaric molar heat capacity,Isochoric molar heat capacity,Isobaric volumetric heat capacity,Isochoric molar-atom heat capacity over R
Air (Sea level dry 0 °C (273.15 K)),gas,1.0035,29.07,20.7643,0.001297,1.25
Air (typical room conditions),gas,1.012,29.19,20.85,0.00121,1.25
Aluminium,solid,0.897,24.2,,2.422,2.91
Ammonia,liquid,4.700,80.08,,3.263,3.21
Antimony,solid,0.207,25.2,,1.386,3.03
Argon,gas,0.5203,20.7862,12.4717,,1.50
Arsenic,solid,0.328,24.6,,1.878,2.96
Beryllium,solid,1.82,16.4,,3.367,1.97
Bismuth[3],solid,0.123,25.7,,1.20,3.09
Cadmium,solid,0.231,26.02,,,3.13
Carbon dioxide CO2[4],gas,0.839,36.94,28.46,,1.14
Chromium,solid,0.449,23.35,,,2.81
Copper,solid,0.385,24.47,,3.45,2.94
Diamond,solid,0.5091,6.115,,1.782,0.74
Ethanol,liquid,2.44,112,,1.925,1.50
Gasoline (octane),liquid,2.22,228,,1.64,1.05
Glass[3],solid,0.84,,,2.1 ,
Gold,solid,0.129,25.42,,2.492,3.05
Granite[3],solid,0.790,,,2.17,
Graphite,solid,0.710,8.53,,1.534,1.03
Helium,gas,5.1932,20.7862,12.4717,,1.50
Hydrogen,gas,14.30,28.82,,,1.23
Hydrogen sulfide H2S[4],gas,1.015,34.60,,,1.05
Lead,solid,0.129,26.4,,1.44,3.18
Lithium,solid,3.58,24.8,,1.912,2.98
Lithium at 181 °C[6],liquid,4.379,30.33,,2.242,3.65
Magnesium,solid,1.02,24.9,,1.773,2.99
Mercury,liquid,0.1395,27.98,,1.888,3.36
Methane at 2 °C,gas,2.191,35.69,,,0.85
Methanol[7],liquid,2.14,68.62,,,1.38
Molten salt (142–540 °C)[8],liquid,1.56,,,2.62,
Nitrogen,gas,1.040,29.12,20.8,,1.25
Neon,gas,1.0301,20.7862,12.4717,,1.50
Oxygen,gas,0.918,29.38,21.0,,1.26
C25H52,solid,2.5 (ave),900,,2.325,1.41
Polyethylene (rotomolding grade)[9][10],solid,2.3027,,,,
Silica (fused),solid,0.703,42.2,,1.547,1.69
Silver[3],solid,0.233,24.9,,2.44,2.99
Sodium,solid,1.230,28.23,,,3.39
Steel,solid,0.466,,,3.756,
Tin,solid,0.227,27.112,,1.659,3.26
Titanium,solid,0.523,26.060,,2.6384,3.13
Tungsten[3],solid,0.134,24.8,,2.58,2.98
Uranium,solid,0.116,27.7,,2.216,3.33
Water at 100 °C (steam),gas,2.080,37.47,28.03,,1.12
Water at 25 °C,liquid,4.1813,75.327,74.53,4.1796,3.02
Water at 100 °C,liquid,4.1813,75.327,74.53,4.2160,3.02
Water at −10 °C (ice)[3],solid,2.05,38.09,,1.938,1.53
Zinc[3],solid,0.387,25.2,,2.76,3.03 
