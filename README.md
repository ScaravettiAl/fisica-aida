These notebooks can be executed live on [binder](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Ffisica-aida.git/HEAD)

If you want to execute the notebooks on your computer, make sure `git` and the `venv` python module are installed and follow the instructions below.

From a terminal, clone this repository
```
git clone https://framagit.org/coslo/fisica-aida.git
cd fisica-aida
```

Install required python packages in a python virtual environment
```
python3 -m venv env
. env/bin/activate
pip install -r requirements.txt
```

Get started with jupyter notebooks
```
jupyter notebook
```

